import React from 'react';
import { HelloWorld } from 'esp-login';

const App = ()  => {

    return (
        <HelloWorld />
    );
}

export default App;